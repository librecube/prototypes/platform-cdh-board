import time
import spacecan
import spacecan.pus


controller = spacecan.Controller(hb_period=0.5, sync_period=5)
pus = spacecan.pus.PacketUtilizationService(controller)
controller.connect(interface="socketcan", channel_a="can0", channel_b="can0")
controller.start()


# print on the screen all incoming packets
def packet_monitor(packet, node_id):
    print(f"received {packet.service, packet.subtype, packet.data.hex()} from {node_id}")


# print the verification report for sent packets
# def request_verification_handler(packet, node_id):
#     if packet.subtype == 1:
#         print(f"success acceptance for {packet.data[0], packet.data[1]} from {node_id}")
#     elif packet.subtype == 2:
#         print(f"fail acceptance for {packet.data[0], packet.data[1]} from {node_id}")
#     elif packet.subtype == 7:
#         print(f"success completion for {packet.data[0], packet.data[1]} from {node_id}")
#     elif packet.subtype == 8:
#         print(f"fail completion for {packet.data[0], packet.data[1]} from {node_id}")


# print received housekeeping packets
def housekeeping_service_handler(packet, node_id):
    print()
    for p, v in pus.parameter_management_service.parameter_pool.items():
        print(p, v.parameter_name, v.value)
    print()


pus.packet_monitor = packet_monitor
# pus.request_verification_service.callback = request_verification_handler
# pus.housekeeping_service.callback = housekeeping_service_handler
pus.parameter_management_service.add_parameters_from_file(
    "node_1/parameters.json", node_id=1)
pus.parameter_management_service.add_parameters_from_file(
    "node_2/parameters.json", node_id=2)
pus.housekeeping_service.add_housekeeping_report_structures_from_file(
    "node_1/housekeeping.json", node_id=1)
pus.housekeeping_service.add_housekeeping_report_structures_from_file(
    "node_2/housekeeping.json", node_id=2)


intro_text = """
    <Stop program with Ctrl-C>

    Enter one of following:

        "b": bus switch
        "s": send scet
        "u": send utc

        x1, x2, x3, [...]
            x1: destination id
            x2: service type
            x3: service subtype
            [...]: comma seperated data values

            Services:
            17, 1   connection test
            3, 5    enable hk report
            3, 6    disable hk report
            3, 27   request hk snapshot
            8, 1    perform function

            Examples:
            1, 17, 1     -> connection test to node 1
            3, 3, 5, 2   -> enable HK report 2 of node 3

    Enter 'h' to show this text again.
"""
print(intro_text)


try:
    while True:
        x = input("").lower()

        if x == "h":
            print(intro_text)

        elif x == "b":
            print("switch bus")
            controller.switch_bus()

        elif x == "s":
            print("send scet")
            coarse_time = int(time.monotonic())
            fine_time = 0
            controller.send_scet(coarse_time, fine_time)

        elif x == "u":
            print("send utc")
            day = int(time.time() / 60 / 60 / 24)  # days since 1970-01-01
            ms_of_day = 0
            sub_ms_of_day = 0
            controller.send_utc(day, ms_of_day, sub_ms_of_day)

        else:
            # the remaining inputs must be comma seperated numbers
            try:
                e = [int(n) for n in x.split(",")]
                node_id = e[0]
                service = e[1]
                subtype = e[2]
                if len(e) > 3:
                    data = e[3:]
                else:
                    data = None
            except (ValueError, IndexError):
                print("Invalid input")
                continue

            packet = spacecan.Packet(service, subtype, data)
            pus.send(packet, node_id)

except KeyboardInterrupt:
    print()

controller.stop()
controller.disconnect()
