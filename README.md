# Platform - Command and Data Handling (CDH) Board

To be written

Raspi Setup:

Set following dtoverlays:

**Raspi 5**

| UART | dtoverlay | AMA  | Pins TX, RX |
| ---- | --------- | ---- | ----------- |
| A    | uart1-pi5 | AMA1 | 27,28       |
| B    | uart2-pi5 | AMA2 | 7,29        |
| C    | uart4-pi5 | AMA4 | 32,33       |
| D    | uart0-pi5 | AMA0 | 8,10        |

**Raspi 4**

| UART | dtoverlay | AMA  | Pins TX, RX |
| ---- | --------- | ---- | ----------- |
| A    | uart2     | AMA2 | 27,28       |
| B    | uart3     | AMA3 | 7,29        |
| C    | uart5     | AMA5 | 32,33       |

UART D is not available on Raspi 4

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
